.DEFAULT_GOAL = help
SHELL = /bin/bash
TARGET = spamreqt
MANPAGE = $(TARGET).1.en
CC = gcc
CFLAGS = -std=gnu17 -Wall -Wextra -Werror
DEBUG ?= 0
ifeq ($(DEBUG), 1)
	CFLAGS += -g -Og
else
	CFLAGS += -O3
endif
LDFLAGS = -lcurl
MEMCHECK = valgrind
MCFLAGS = -s --leak-check=full --show-leak-kinds=all --track-origins=yes
RM = rm -f
CONNECTIONS ?= $(shell nproc)
REQUESTS ?= 1
URI ?= b5lab.com
TIMEOUT = 120000
SRFLAGS =  -c $(CONNECTIONS) -r $(REQUESTS) -u $(URI) -t$(TIMEOUT)

.PHONY: help
##@ HELP
help: ## display this help
	@awk 'BEGIN {FS = ":.*##"; \
	printf "\033[1mUSAGE\n\033[0m  make \033[36m<TARGET>\033[0m\n"} \
	/^[a-zA-Z0-9_-]+\.*[a-zA-Z]*:.*?##/ \
	{printf "  \033[36m%-8s\033[0m %s\n", $$1, $$2} \
	/^###/ {printf "\033[0m\t     %s\033[0m\n", substr($$0, 4)} \
	/^##@/ {printf "\n\033[1m%s\033[0m\n", substr($$0, 5)}' \
	$(MAKEFILE_LIST)

.PHONY: man
##@ MANUAL
man: ## open manual page
	@man ./$(MANPAGE)

.PHONY: demo
##@ DEMO
demo: $(TARGET) ## execute with debug flag and time
	time ./$(<) $(SRFLAGS) -d

.PHONY: build
##@ BUILD
build: $(TARGET) ## build spamreqt
$(TARGET):
	@$(CC) $(CFLAGS) -o $(@) $(@).c $(LDFLAGS)

.PHONY: memcheck clean
##@ EXTRA
memcheck: $(TARGET) ## memory check
	@$(MEMCHECK) $(MCFLAGS) ./$(TARGET) $(SRFLAGS)

clean: ## remove artifacts
	@$(RM) $(TARGET)
