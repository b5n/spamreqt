/*-----------------------------------------------------------------------------
 * File      spamreqt.c
 * Author(s) b5n
 *
 * Purpose   spam requests
 *
 *-----------------------------------------------------------------------------
 * Notes     needs more work in regard to checking return values and handling
 *           possible errors
 *-----------------------------------------------------------------------------
 * Revisions n/a
 *---------------------------------------------------------------------------*/

#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

#define USERAGENT "sr-v0.0.1"

#define SR_STRERROR()                                                          \
  ({                                                                           \
    int err = errno;                                                           \
    err == 0 ? "unspecified" : strerror(err);                                  \
  })

#define SR_PERROR(MSG)                                                         \
  fprintf(stderr, "%s:%d:%s %s: %s\n", __FILE__, __LINE__, __func__, MSG,      \
          SR_STRERROR())

typedef struct {
  bool debug;
  bool insecure;
  bool progress;
  long connections;
  long requests;
  long timeout;
  long bufsize;
  char* uri;
} args;

typedef struct {
  size_t size;
  char* buf;
} memory;

int interrupt = 0;
static void
signal_handler(int sig) {
  switch (sig) {
  case SIGINT:
  case SIGQUIT:
  case SIGABRT:
  case SIGTERM:
  case SIGTSTP:
  default:
    fprintf(stderr, "signal:%d shutting down\n", sig);
    interrupt = 1;
    break;
  }
}

static int
register_signals(void) {
  int signals[] = {SIGINT, SIGQUIT, SIGABRT, SIGTERM, SIGTSTP};
  int len = sizeof(signals) / sizeof(signals[0]);

  for (int i = 0; i < len; i++) {
    if (signal(signals[i], signal_handler) == SIG_ERR) {
      SR_PERROR("signal");
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}

static args
get_args(int argc, char** argv) {
  int opt;
  int optsi;
  args args = {0};
  args.timeout = 60000;
  args.bufsize = 0;
  char* sopts = "dipc:r:u:t::b::";
  static struct option lopts[] = {{"debug", no_argument, NULL, 'd'},
                                  {"insecure", no_argument, NULL, 'i'},
                                  {"progress", no_argument, NULL, 'p'},
                                  {"connections", required_argument, 0, 'c'},
                                  {"requests", required_argument, 0, 'r'},
                                  {"uri", required_argument, 0, 'u'},
                                  {"timeout", optional_argument, 0, 't'},
                                  {"bufsize", optional_argument, 0, 'b'},
                                  {0, 0, 0, 0}};

  /* validation and error handling can be improved */
  while ((opt = getopt_long(argc, argv, sopts, lopts, &optsi)) != -1) {
    optsi = 0;
    switch (opt) {
    case 'd':
      args.debug = true;
      break;
    case 'i':
      args.insecure = true;
      break;
    case 'p':
      args.progress = true;
      break;
    case 'c':
      if ((args.connections = atol(optarg)) == 0) {
        fputs("-m, --max-connections error: exiting\n", stderr);
        exit(EXIT_FAILURE);
      }
      break;
    case 'r':
      if ((args.requests = atol(optarg)) == 0) {
        fputs("-r, --requests error: exiting\n", stderr);
        exit(EXIT_FAILURE);
      }
      break;
    case 'u':
      args.uri = optarg;
      break;
    case 't':
      if (optarg == NULL || (args.timeout = atol(optarg)) == 0) {
        fputs("-t, --timeout error: exiting\n", stderr);
        exit(EXIT_FAILURE);
      }
      break;
    case 'b':
      if (optarg == NULL || (args.bufsize = atol(optarg)) == 0) {
        fputs("-b, --bufsize error: exiting\n", stderr);
        exit(EXIT_FAILURE);
      }
      break;
    /* default unreachable due to getopt */
    default:
      fputs("unreachable, exiting\n", stderr);
      exit(EXIT_FAILURE);
    }
  }

  return args;
}

/* could have finer grained control over debug output */
/* such as enabling hex, text, or both */
static void
curl_dump(const char* text, FILE* stream, unsigned char* ptr, size_t size) {
  size_t i;
  size_t c;
  unsigned int width = 0x10;

  if (text != NULL) {
    fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n", text, (long)size,
            (long)size);
  }

  for (i = 0; i < size; i += width) {
    fprintf(stream, "%4.4lx: ", (long)i);

    /* show hex to the left */
    for (c = 0; c < width; c++) {
      if (i + c < size)
        fprintf(stream, "%02x ", ptr[i + c]);
      else
        fputs("   ", stream);
    }

    /* show data on the right */
    for (c = 0; (c < width) && (i + c < size); c++) {
      char x = (ptr[i + c] >= 0x20 && ptr[i + c] < 0x80) ? ptr[i + c] : '.';
      fputc(x, stream);
    }

    fputc('\n', stream);
  }
}

static int
curl_debug(CURL* handle, curl_infotype type, char* data, size_t size,
           void* clientp) {
  (void)handle;
  (void)clientp;

  const char* text = NULL;
  switch (type) {
  case CURLINFO_TEXT:
    fprintf(stderr, "== info: %s", data);
    fwrite(data, size, 1, stderr);
    break;
  case CURLINFO_HEADER_OUT:
    text = "=> send header";
    break;
  case CURLINFO_DATA_OUT:
    text = "=> send data";
    break;
  case CURLINFO_SSL_DATA_OUT:
    text = "=> send ssl data";
    break;
  case CURLINFO_HEADER_IN:
    text = "<= recv header";
    break;
  case CURLINFO_DATA_IN:
    text = "<= recv data";
    break;
  case CURLINFO_SSL_DATA_IN:
    text = "<= recv ssl data";
    break;
  default:
    text = "== unspecified";
  }

  if (data != NULL) {
    curl_dump(text, stderr, (unsigned char*)data, size);
  } else {
    fprintf(stderr, "%s: [NULL data, size=%zu]\n", text, size);
  }

  return EXIT_SUCCESS;
}

static size_t
curl_write(char* data, size_t size, size_t nmemb, void* udata) {
  size_t dsize = size * nmemb;
  memory* mem = (memory*)udata;
  char* rv;

  if (!(rv = realloc(mem->buf, mem->size + dsize))) {
    SR_PERROR("realloc");
    fputs("exiting", stderr);
    exit(EXIT_FAILURE);
  }

  mem->buf = rv;
  memcpy(&(mem->buf[mem->size]), data, dsize);
  mem->size += dsize;

  return dsize;
}

static CURL*
init_handle(args* args) {
  CURL* handle = curl_easy_init();

  memory* mem = calloc(1, sizeof(memory));
  mem->size = 0;
  mem->buf = calloc(1, sizeof(char));

  if (args->debug) {
    curl_easy_setopt(handle, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(handle, CURLOPT_DEBUGFUNCTION, curl_debug);
  }

  if (args->insecure) {
    curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 0L);
  }

  if (args->progress) {
    curl_easy_setopt(handle, CURLOPT_NOPROGRESS, 0L);
  }

  /* only needs to be set on a single handle */
  /* will be applied to ALL handles associated with the same multi handle */
  if (args->bufsize) {
    curl_easy_setopt(handle, CURLOPT_BUFFERSIZE, args->bufsize);
    args->bufsize = 0;
  }

  curl_easy_setopt(handle, CURLOPT_TCP_KEEPALIVE, 1L);
  curl_easy_setopt(handle, CURLOPT_DNS_CACHE_TIMEOUT, 60L);
  curl_easy_setopt(handle, CURLOPT_KEEP_SENDING_ON_ERROR, 1L);
  curl_easy_setopt(handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2TLS);
  curl_easy_setopt(handle, CURLOPT_URL, args->uri);
  curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, curl_write);
  curl_easy_setopt(handle, CURLOPT_WRITEDATA, mem);
  curl_easy_setopt(handle, CURLOPT_PRIVATE, mem);
  curl_easy_setopt(handle, CURLOPT_ACCEPT_ENCODING, "");
  curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(handle, CURLOPT_REDIR_PROTOCOLS_STR, "http,https");
  curl_easy_setopt(handle, CURLOPT_AUTOREFERER, 1L);
  curl_easy_setopt(handle, CURLOPT_MAXREDIRS, 10L);
  curl_easy_setopt(handle, CURLOPT_TIMEOUT_MS, args->timeout);
  curl_easy_setopt(handle, CURLOPT_CONNECTTIMEOUT_MS, args->timeout >> 2);
  curl_easy_setopt(handle, CURLOPT_COOKIEFILE, "");
  curl_easy_setopt(handle, CURLOPT_FILETIME, 1L);
  curl_easy_setopt(handle, CURLOPT_USERAGENT, USERAGENT);
  curl_easy_setopt(handle, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
  curl_easy_setopt(handle, CURLOPT_UNRESTRICTED_AUTH, 1L);
  curl_easy_setopt(handle, CURLOPT_PROXYAUTH, CURLAUTH_ANY);
  curl_easy_setopt(handle, CURLOPT_EXPECT_100_TIMEOUT_MS, 0L);
  curl_easy_setopt(handle, CURLOPT_MAXFILESIZE_LARGE,
                   (curl_off_t)1024 * 1024 * 1024);

  return handle;
}

int
main(int argc, char** argv) {
  if (register_signals()) {
    fputs("exiting", stderr);
    exit(EXIT_FAILURE);
  }

  args args = get_args(argc, argv);

  curl_global_init(CURL_GLOBAL_DEFAULT);
  CURLM* multi_handle = curl_multi_init();
  curl_multi_setopt(multi_handle, CURLMOPT_MAX_TOTAL_CONNECTIONS,
                    args.connections);
  curl_multi_setopt(multi_handle, CURLMOPT_MAX_HOST_CONNECTIONS,
                    args.connections);

#ifdef CURLPIPE_MULTIPLEX
  curl_multi_setopt(multi_handle, CURLMOPT_PIPELINING, CURLPIPE_MULTIPLEX);
#endif

  for (int i = 0; i <= args.requests; i++) {
    curl_multi_add_handle(multi_handle, init_handle(&args));
  }

  int nready;
  int complete = 0;
  int nrunning = 1;
  while (!interrupt && nrunning) {
    int nfds;
    curl_multi_wait(multi_handle, NULL, 0, args.timeout << 1, &nfds);
    curl_multi_perform(multi_handle, &nrunning);
    CURLMsg* m = NULL;

    while (!interrupt && (m = curl_multi_info_read(multi_handle, &nready))) {
      if (m->msg == CURLMSG_DONE) {
        CURL* handle = m->easy_handle;
        memory* mem;
        curl_easy_getinfo(handle, CURLINFO_PRIVATE, &mem);

        if (m->data.result != CURLE_OK) {
          long res_status;
          curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &res_status);
          printf("[%d]:status:%ld CURLcode:%d %s\n", complete, res_status,
                 m->data.result, curl_easy_strerror(m->data.result));

          if (m->data.result == 28) {
            fputs("remaining requests unlikely to complete, try increasing "
                  "timeout: -t100000\nshutting down\n",
                  stderr);
            interrupt = 1;
          }
        }

        curl_multi_remove_handle(multi_handle, handle);
        curl_easy_cleanup(handle);
        free(mem->buf);
        free(mem);
        complete++;
      }
    }
  }

  curl_multi_cleanup(multi_handle);
  curl_global_cleanup();
  return EXIT_SUCCESS;
}
